import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class New_Table extends JPanel{
    String[] columnNames;
    DefaultTableModel model;
    JTable table;
    public New_Table(String[] columnNames){
        super();
        this.columnNames = columnNames;
        table = new JTable();
        JScrollPane scrollBar = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        model = new DefaultTableModel(){
            public boolean isCellEditable(int rowIndex, int mColIndex) {
                return false;
            }
        };
        model.setColumnIdentifiers(columnNames);

        table.setModel(model);
        this.add(scrollBar);

    }

    String[] getColumnNames(){
        return columnNames;
    }

    void addRow(String[] contentToPut){
        model.addRow(contentToPut);
    }

    void initialiseNew(ValueSaver valueSaver){
        model.setColumnIdentifiers(valueSaver.columnNames);
        this.columnNames = valueSaver.columnNames;
        for(String[] row : valueSaver.rows){
            model.addRow(row);
        }
    }

}
