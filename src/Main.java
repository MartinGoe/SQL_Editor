import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import java.util.stream.Collectors;

public class Main extends JFrame{
    private DBConnection connection;
    private HashMap<String, New_Table> tableHashMap = new HashMap<>();
    private Main() {
        super();

        String host = JOptionPane.showInputDialog(null, "What is the host?");
        String database = JOptionPane.showInputDialog(null, "What is the database called?");
        String username = JOptionPane.showInputDialog(null, "What is the username?");
        String password = JOptionPane.showInputDialog(null, "What is the password?");

        try {
            connection = new DBConnection(database, username, password, host);
            System.out.println(connection.isValid());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        JTabbedPane tabbedPane = new JTabbedPane();

        JPanel actionPan = new JPanel();
        //{
        JButton addTable = new JButton("New Table");
        JButton removeTable = new JButton("Remove Table");

        JButton addRow = new JButton("Add Row");
        JButton removeRow = new JButton("Remove Row");
        //}


        ResultSet resultSet = connection.execute("SHOW TABLES FROM " + DBConnection.database + ";");
        try {
            while (resultSet.next()) {
                String s = resultSet.getString(1);
                System.out.println(s);
                ResultSet rs = connection.execute("SELECT * FROM " + s + ";");

                ResultSetMetaData rsmd = rs.getMetaData();

                int columnsNumber = rsmd.getColumnCount();

                ArrayList<String[]> rows = new ArrayList<>();
                while (rs.next()){
                    String[] row = new String[columnsNumber];
                    for(int i = 1; i < columnsNumber + 1; i++){
                        row[i-1] = rs.getString(i);
                    }
                    rows.add(row);
                }
                String[] columnNames = new String[columnsNumber];

                for(int j = 0; j < columnsNumber; j++){
                    columnNames[j] = rsmd.getColumnLabel(j + 1);
                }
                String[] d = {};
                New_Table table = new New_Table(d);
                ValueSaver valueSaver = new ValueSaver(s, columnNames, rows);
                table.initialiseNew(valueSaver);
                tableHashMap.put(s, table);
                tabbedPane.add(s, table);

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        addTable.addActionListener(e -> {
            try {
                String name = JOptionPane.showInputDialog(null, "What should the Name be?").toLowerCase();
                int columnCount = Integer.parseInt(JOptionPane.showInputDialog(null, "How many columns should it have?"));
                String[] columnNames = new String[columnCount];
                for (int i = 0; i < columnCount; i++)
                    columnNames[i] = (JOptionPane.showInputDialog(null, "What should the " + (i + 1) + ". column be called?") + " varchar(255)").toLowerCase();

                String queryColumnnames = Arrays.stream(columnNames).collect(Collectors.joining(", "));
                connection.update(String.format("CREATE TABLE %s (%s);", name, queryColumnnames));

                String[] showableNames = new String[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    String s = columnNames[i];
                    s = s.replace("varchar(255)", "").replace(",", "").replace("'", "").replace(" ", "");
                    showableNames[i] = s;
                }

                New_Table table = new New_Table(showableNames);
                tableHashMap.put(name, table);
                tabbedPane.add(name, table);
            }
            catch (Exception e1){
                e1.printStackTrace();
            }
        });


        removeTable.addActionListener(e -> {
            int index = tabbedPane.getSelectedIndex();

            String tableName = tabbedPane.getTitleAt(index);

            connection.update("DROP TABLE " + tableName + ";");

            tabbedPane.remove(index);
        });


        addRow.addActionListener(e -> {
            int index = tabbedPane.getSelectedIndex();

            String tableName = tabbedPane.getTitleAt(index);

            String[] columnNames = tableHashMap.get(tableName).getColumnNames();
            int columnCount = columnNames.length;

            String[] contentToPut = new String[columnNames.length];

            for(int i = 0; i < columnCount; i++){
                String s = JOptionPane.showInputDialog(null, String.format("What should the %ss column be?", columnNames[i])).toLowerCase();
                contentToPut[i] = s;
            }

            String queryContent = Arrays.stream(contentToPut).collect(Collectors.joining(", "));

            System.out.println(queryContent + "\n" + tableName);
            connection.update("INSERT INTO " + tableName + " VALUES (" + getSymbols(columnCount) + ");", contentToPut);
            tableHashMap.get(tableName).addRow(contentToPut);
        });


        removeRow.addActionListener(e -> {
            try {
                int index = tabbedPane.getSelectedIndex();

                String tableName = tabbedPane.getTitleAt(index);

                New_Table tableClass = tableHashMap.get(tableName);
                String[] columnNames = tableClass.getColumnNames();
                int columnCount = columnNames.length;

                String[] columns = new String[columnCount];


                int row = tableClass.table.getSelectedRow();
                System.out.println(row);
                for (int i = 0; i < columnCount; i++) {
                    columns[i] = tableClass.model.getValueAt(row, i).toString();
                }
                String[] queryWhere = new String[columnCount];

                for (int i = 0; i < columnCount; i++) {
                    queryWhere[i] = columnNames[i] + "='" + columns[i] + "'";
                }
                String query = "DELETE FROM " + tableName + " WHERE " + Arrays.stream(queryWhere).collect(Collectors.joining(" AND ")) + ";";
                System.out.println(query);
                connection.update(query);
                tableClass.model.removeRow(row);
            }catch (Exception e1){
                e1.printStackTrace();
            }

        });

        actionPan.add(addTable);
        actionPan.add(removeTable);
        actionPan.add(addRow);
        actionPan.add(removeRow);

        this.setLayout(new BorderLayout());

        this.add(tabbedPane, BorderLayout.CENTER);
        this.add(actionPan, BorderLayout.SOUTH);
        this.setTitle("Database - " + database);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(700, 600);
        this.setVisible(true);
    }


    public static void main(String[] args) {
        JFrame frame = new Main();

    }


    static String getSymbols(int amount){
        String s = "";
        for(int i = 0; i < amount; i++){
            s += "?, ";
        }
        return s.substring(0, s.length() - 2);
    }
}
